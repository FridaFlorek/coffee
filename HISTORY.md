
2.1.0 / 2015-11-17
==================

  * feat: export this.proc

2.0.0 / 2015-10-31
==================

  * feat: easy to intergrate with instanbul
  * deps: upgrade childprocess to 2

1.3.1 / 2015-10-29
==================

  * fix: callback trigger twice when it throws

1.3.0 / 2015-10-29
==================

  * feat: pass stdout, stderr and code in end callback

1.2.0 / 2015-09-01
==================

  * feat: support options.autoCoverage to enable code coverage with istanbul
  * chore: eslint instead of jshint
  * test: coverage 100%
  * feat: add codecov.io
  * doc(readme): fixed example code

1.1.0 / 2015-05-23
==================

- feat: expect error
- chore: use pkg.files
- chore: use pkg.scripts
- feat: Write data to process.stdout and process.stderr for debug
- fix coveralls image

1.0.1 / 2015-05-15
==================

- fix: condition when use stdio: inherit

1.0.0
==================

First commit
